# TurnTable.fm Song Tracker Extension

Track what songs you are listening to in a TurnTable.fm room!

This package contains to pieces: a Google Chrome extension that runs in the background of your browser while you are listening in a TurnTable.fm room, and a PHP script that accepts the logging calls from the Chrome extension.

Client requirements:
 - Google Chrome

Server requirements:
 - PHP with MongoDB extensions
 - MongoDB
