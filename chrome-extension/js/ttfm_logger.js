
// Set the URL of your api-downloader 
var tt_dl_api_url = "turntable/"

/*
 * object.watch polyfill
 *
 * 2012-04-03
 *
 * By Eli Grey, http://eligrey.com
 * Public Domain.
 * NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
 */
 
// object.watch
if (!Object.prototype.watch) {
    Object.defineProperty(Object.prototype, "watch", {
          enumerable: false
        , configurable: true
        , writable: false
        , value: function (prop, handler) {
            var
              oldval = this[prop]
            , newval = oldval
            , getter = function () {
                return newval;
            }
            , setter = function (val) {
                oldval = newval;
                return newval = handler.call(this, prop, oldval, val);
            }
            ;
            
            if (delete this[prop]) { // can't watch constants
                Object.defineProperty(this, prop, {
                      get: getter
                    , set: setter
                    , enumerable: true
                    , configurable: true
                });
            }
        }
    });
}
 
// object.unwatch
if (!Object.prototype.unwatch) {
    Object.defineProperty(Object.prototype, "unwatch", {
          enumerable: false
        , configurable: true
        , writable: false
        , value: function (prop) {
            var val = this[prop];
            delete this[prop]; // remove accessors
            this[prop] = val;
        }
    });
}

function log_song() {
    console.log("Logging song...");
    $.ajax({
        type: "POST",
        contentType: 'application/json',
        url: tt_dl_api_url + "log_track.php",
        data: JSON.stringify({
            room: {
                id: window.TURNTABLE_ROOM.roomid,
                name: $(".header-room-name")[0].innerHTML,
                short_name: window.TURNTABLE_ROOM.shortcut
            },
            song: window.song
        }),
        success: function(e) {
            console.log("Server responded!");
        }
    });
}

function log_song(comment) {
    console.log("Logging song...");
    var outbound_json = {
            room: {
                id: window.TURNTABLE_ROOM.roomid,
                name: $(".header-room-name")[0].innerHTML,
                short_name: window.TURNTABLE_ROOM.shortcut
            },
            song: window.song
        };
    outbound_json.song.comment = comment;
    $.ajax({
        type: "POST",
        contentType: 'application/json',
        url: tt_dl_api_url + "log_track.php",
        data: JSON.stringify(outbound_json),
        success: function(e) {
            console.log("Server responded!");
        }
    });
}

setTimeout( function() {
    console.log("Watcher Loaded.");
    log_song();
    window.turntable.watch("current_songid", function (id, oldval, newval) {
        log_song();
    });
}, 10000);
