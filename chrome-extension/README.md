# TurnTable.fm Logger Chrome Extension


### Install Instructions

1. Clone the git repo to a folder on your local system.
2. In ttfm_logger.js, set `tt_dl_api_url` to the URL of your server hosting the api-downloader folder.
3. In your Google Chrome browser, navigate to the Extensions menu.  (This is located under Settings > Tools > Extensions.)
4. Click the "Load unpacked extension..." and select the folder where the repo was cloned.  (If the button to do this is missing, make sure that the "Developer Mode" is checked.)


That's it!  You're good to go!  From now on, when you're on TurnTable.fm, your browser will automatically ping the the API and log what song is on.
