<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);
require_once("mongodb.config.php");

$m = new MongoClient("mongodb://".$mongo_username.":".$mongo_password."@".$mongo_url.":".$mongo_port."/".$mongo_database);
$ttdb = $m->selectDB("turntable");
$tracks = ($ttdb->tracks->find()->sort(array('starttime' => -1))->limit(100));

function get_length($length) {
    return floor($length/60).":".str_pad(($length % 60),2,"0",STR_PAD_LEFT);
}


?><html>
<head>
    <title>TurnTable Tracker</title>
    <style>
        body {
            font-family:Arial, Helvetica, sans-serif;
            background-color: #000;
        }
        .song {
            color:#fff;
            width:95%;
            height:70px;
            border:1px solid #000;
            padding: 10px;
            margin: 0 auto;
            background: #a90329; /* Old browsers */
            background: -moz-linear-gradient(top, #a90329 0%, #8f0222 44%, #6d0019 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#a90329), color-stop(44%,#8f0222), color-stop(100%,#6d0019)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, #a90329 0%,#8f0222 44%,#6d0019 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, #a90329 0%,#8f0222 44%,#6d0019 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, #a90329 0%,#8f0222 44%,#6d0019 100%); /* IE10+ */
            background: linear-gradient(to bottom, #a90329 0%,#8f0222 44%,#6d0019 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a90329', endColorstr='#6d0019',GradientType=0 ); /* IE6-9 */
        }
        .song a { 
            color:#fff;
            text-decoration: none;
        }
        .song_info {
            /* background-color:#f00; */
            height:70px;
            width:80%;
            float:left;
        }
        .song_actions {
            /* background-color:#ff0; */
            height:70px;
            width:19%;
            float:right;
        }
        .song_action_icon {
            height:50px;
            margin-top:10px;
        }
    </style>
</head>
<body>
    <?php
    //$tracks = iterator_to_array($tracks);
    //for ($i = 0; $i < count($tracks); $i++) {
    foreach ($tracks as $track) {
    ?>
    <div class="song">
        <div class="song_info">
            <a href="http://turntable.fm/link/?fileid=<?=$track["_id"] ?>&site=soundcloud"><font style="font-size:15px; font-weight:bold; overflow:hidden;"><?=$track["metadata"]["song"] ?></font></a><br />
            <font style="font-size:12px;"><?=$track["metadata"]["artist"] ?> (<?=get_length($track["metadata"]["length"]) ?>)</font><br />
            <font style="font-size:11px;">Played in <a href="http://turntable.fm/<?=$track["room"]["short_name"] ?>" target="_blank"><?=$track["room"]["name"] ?></a></font><br />
            <font style="font-size:10px;">Played at <?=date("r", $track["starttime"]) ?></font><?php if (isset($track["comment"])) { ?><br /><font style="font-size:10px; color:yellow;">Comments: <?=$track["comment"] ?></font><? } ?>
        </div>
        <div class="song_actions">
            <a href="download.php?track_id=<?=$track["_id"] ?>" target="_blank" style="float:right;"><img src="images/download.png" class="song_action_icon" /></a>
            <!-- <a href="http://turntable.fm/link/?fileid=<?=$track["_id"] ?>&site=soundcloud">
                <img src="images/soundcloud-logo.png" class="song_action_icon" />
            </a> -->
        </div>
    </div>
    <?php
    }
    ?>
</body>
</html>
