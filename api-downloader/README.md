# TurnTable.fm API/Downloader Script

This bundle includes the required files to log songs playing in a TurnTable.fm room and download (where available) the songs from SoundCloud.

To get this package working:
 - copy `soundcloud_api.config.template.php` as `soundcloud_api.config.php` and fill in the required variable with your SoundCloud API key.
 - copy `mongodb.config.template.php` as `mongodb.config.php` and fill in the required variables with your MongoDB location and credentials.

The server these scripts will run on requires:
 - PHP with MongoDB extensions
 - MongoDB
