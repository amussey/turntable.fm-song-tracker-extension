<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);
require_once("mongodb.config.php");

$m = new MongoClient("mongodb://".$mongo_username.":".$mongo_password."@".$mongo_url.":".$mongo_port."/".$mongo_database);
$ttdb = $m->selectDB("turntable");
$json = file_get_contents("php://input");
file_put_contents("out.txt", $json);

try {
    $json = json_decode($json, true);
    if ($json == "") {
        throw new Exception('JSON parsing failed.');
    }
} catch (Exception $e) {
    echo 'JSON parsing failed.';
    exit;
}

if (!isset($json["song"])) {
    die("Song information missing.");
}

if (!isset($json["room"])) {
    die("Room information missing.");
}

echo "Looks good!";
print_r($json);

$json["song"]["room"] = $json["room"];

$ttdb->tracks->save($json["song"]);
