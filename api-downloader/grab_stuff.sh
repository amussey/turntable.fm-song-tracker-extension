#!/bin/bash

CLIENT_ID=$2

URL_CURL=$1
SOUNDCLOUD_URL=$(curl "http://turntable.fm/link/?fileid=$URL_CURL&site=soundcloud" -s -L -I -o /dev/null -w '%{url_effective}')
SOUNDCLOUD_URL=$(curl "http://api.soundcloud.com/resolve.json?url=$SOUNDCLOUD_URL&client_id=$CLIENT_ID" -s)

php grab_stuff.php "$CLIENT_ID" "$SOUNDCLOUD_URL"
