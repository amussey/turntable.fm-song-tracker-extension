<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);
require_once("mongodb.config.php");
require_once("soundcloud_api.config.php")

$out = array();
exec('./grab_stuff.sh '.$_GET['track_id'].' '.$soundcloud_api.' 2>&1', $out);
if ($out[0] == "No good.") {
    die("This track is currently not available on SoundCloud.");
}
$filename = $_GET["track_id"].".mp3";
file_put_contents("mp3s/".$filename, file_get_contents($out[0]));

$m = new MongoClient("mongodb://".$mongo_username.":".$mongo_password."@".$mongo_url.":".$mongo_port."/".$mongo_database);
$ttdb = $m->selectDB("turntable");
$tag_information = iterator_to_array($ttdb->tracks->find(array("_id" => $_GET["track_id"])));
$tag_information_temp = $tag_information;
foreach ($tag_information_temp as $tag) {
    $tag_information = $tag;
}


/*
var_dump(id3_get_tag ("mp3s/".$filename));

if (id3_set_tag("mp3s/".$filename, array(
    "title"  => substr((isset($tag_information["metadata"]["song"]) ? $tag_information["metadata"]["song"] : ""), 0, 30),
    "artist" => substr((isset($tag_information["metadata"]["artist"]) ? $tag_information["metadata"]["artist"] : ""), 0, 30),
    "album"  => substr((isset($tag_information["metadata"]["album"]) ? $tag_information["metadata"]["album"] : ""), 0, 30)
))) {
    die("Tagging failed.");
}*/


// Now tagging
$TextEncoding = 'UTF-8';

require_once('./id3/getID3-1.9.7/getid3/getid3.php');
// Initialize getID3 engine
$getID3 = new getID3;
$getID3->setOption(array('encoding'=>$TextEncoding));

require_once('./id3/getID3-1.9.7/getid3/write.php');
// Initialize getID3 tag-writing module
$tagwriter = new getid3_writetags;
//$tagwriter->filename = '/path/to/file.mp3';
$tagwriter->filename = 'mp3s/'.$filename;

$tagwriter->tagformats = array('id3v1', 'id3v2.3');
//$tagwriter->tagformats = array('id3v2.3');

// set various options (optional)
$tagwriter->overwrite_tags = true;
//$tagwriter->overwrite_tags = false;
$tagwriter->tag_encoding = $TextEncoding;
$tagwriter->remove_other_tags = false;

// populate data array
$TagData = array(
    'title'         => array((isset($tag_information["metadata"]["song"]) ? $tag_information["metadata"]["song"] : "")),
    'artist'        => array((isset($tag_information["metadata"]["artist"]) ? $tag_information["metadata"]["artist"] : "")),
    'album'         => array((isset($tag_information["metadata"]["album"]) ? $tag_information["metadata"]["album"] : "")),
    'comment'       => array('Found in the TurnTable.fm room '.$tag_information["room"]["name"])
);
$tagwriter->tag_data = $TagData;

// write tags
if ($tagwriter->WriteTags()) {
    // echo 'Successfully wrote tags<br>';
    // if (!empty($tagwriter->warnings)) {
    //     echo 'There were some warnings:<br>'.implode('<br><br>', $tagwriter->warnings);
    // }
    // Good to go!
} else {
    die('Failed to write tags!<br>'.implode('<br><br>', $tagwriter->errors));
}

// Set headers
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=\"".$TagData['artist'][0].' - '.$TagData['title'][0].".mp3\"");
header("Content-Type: audio/mpeg3");
header("Content-Transfer-Encoding: binary");
header('Content-Length: ' . filesize("mp3s/".$filename));
readfile("mp3s/".$filename);
